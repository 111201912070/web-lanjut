<!DOCTYPE html>
<html lang="en">

<body>
    <!-- echo -->
    <?php
    $teks = "Ali Ma'ruf senang belajar PHP!<br>";
    echo "<h2>PHP mudah digunakan</h2>";
    echo ("Hello World!<br>");
    echo $teks;
    echo "teks" . "ini" . "dibuat" . "dengan" . "banyak parameter";
    ?>

    <!-- print -->
    <?php
    $txt1 = "PHP";
    $txt2 = "Fakultas Ilmu Komputer";
    $x = 5;
    $y = 4;
    $ret = print("Hello World!");
    print "nilai return=$ret";
    print "<h2>" . $txt1 . "</h2>";
    print "Belajar Pemrograman Web di" . $txt2 . "<br>";
    print $x + $y;
    ?>

    <!-- prin_r -->
    <?php
    $siswa = array('Arif', 'Beta', 'Cici');
    echo '<pre>';
    print_r($siswa);
    echo '</pre>';
    ?>

    <!-- var_dump -->
    <?php
    $nama = 'Agus';
    var_dump($nama); // Hasil:string(4)"Agus"
    $siswa = array(
        'nama' => array('Arif', 'Beta', 'Cici'),
        'jurusan' => 'Informatika',
        'semester' => 1,
        1 => 'Jakarta',
        2 => 'Surabaya'
    );
    echo '<pre>';
    var_dump($siswa);
    echo '</pre>';
    ?>

    <!-- variabel lokal -->
    <?php
    function fungsi1()
    {
        $x = 5; // local scope
        echo "<p>Variablexdi dlm fungsi:$x</p>";
    }
    fungsi1();
    // menggunakan variabelxdi luar fungsi akan mengakibatkan error
    echo "<p>Variablexdi luar fungsi:$x</p>";
    ?>

    <!-- variabel global -->
    <?php
    function fungsi2()
    {
        $x = 5; // local scope
        echo "<p>Variablexdi dlm fungsi:$x</p>";
    }
    fungsi2();
    // menggunakan variabelxdi luar fungsi akan mengakibatkan error
    echo "<p>Variablexdi luar fungsi:$x</p>";
    ?>

    <!-- variabel stativ -->
    <?php
    function fungsi3()
    {
        $x = 5; // local scope
        echo "<p>Variablexdi dlm fungsi:$x</p>";
    }
    fungsi3();
    // menggunakan variabelxdi luar fungsi akan mengakibatkan error
    echo "<p>Variablexdi luar fungsi:$x</p>";
    ?>

    <!-- tipe data string -->
    <?php
    $x = "Hello world!";
    $y = 'Hello world!';
    echo $x;
    echo "<br>";
    echo $y;
    ?>

    <!-- tipe data integer -->
    <?php
    $x = 5985;
    var_dump($x);
    ?>

    <!-- tipe data float -->
    <?php
    $x = 10.365;
    var_dump($x);
    ?>

    <!-- tipe data boolean -->
    <?php
    $x = true;
    $y = false;
    var_dump($x);
    ?>

    <!-- tipe data array -->
    <?php
    $mobil = array("Mitsubishi", "Daihatsu", "Toyota");
    var_dump($mobil);
    ?>

    <!-- tipe data object -->
    <?php
    class Car
    {
        public $color;
        public $model;
        public function __construct($color, $model)
        {
            $this->color = $color;
            $this->model = $model;
        }
        public function message()
        {
            return "My car is a " . $this->color . " " . $this->model . "!";
        }
    }

    $myCar = new Car("black", "Volvo");
    echo $myCar->message();
    echo "<br>";
    $myCar = new Car("red", "Toyota");
    echo $myCar->message();
    ?>

    <!-- tipe data null -->
    <?php
    $x = "Hello world!";
    $x = null;
    var_dump($x);
    ?>

    <!-- tipe data resource -->
    <?php
    $conn = mysqli_connect("localhost", "root", "admin", "buku");
    ?>

    <!-- konstanta -->
    <?php
    define("SALAM", "Selamat Pagi");

    define("mobil", [
        "Honda",
        "Daihatsu",
        "Toyota"
    ]);
    function fungsi4()
    {
        echo  SALAM;
    }
    fungsi1();
    echo mobil[0];
    ?>
    
</body>

</html>