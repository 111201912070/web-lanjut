<html lang="en">

<head>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body class="d-flex h-100 flex-column">
  <!-- Navigation Bar -->
  <nav id="navbar-top" class="navbar navbar-expand-md navbar-light bg-info mb-3">
    <a class="navbar-brand ms-5 fs-2 p-4 fw-bold" href="list-product.php">Tokoku</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample04">
      <ul class="navbar-nav ms-5 me-5 fs-4">
        <li class="nav-item active">
          <a class="nav-link" href="list-product.php ">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="#">Cart</a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- End Navbar -->

  <div class="justify-content-center row-cols-1 row-cols-md-1 g-4 mt-2 mx-4">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nama</th>
          <th>Jumlah</th>
          <th>Harga</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
        session_start();
        $total = 0;
        if (!empty($_SESSION['cart'])) {
          echo "<p>Jumlah data: " . sizeof($_SESSION['cart']['arrCart']) . "</p>";
          $max = sizeof($_SESSION['cart']['arrCart']);

          for ($i = 0; $i < $max; $i++) {
            echo "<tr>";
            foreach ($_SESSION['cart']['arrCart'][$i] as $key => $val) {
              echo "<td>" . $val . "</td>";
            }
            $total = 0;
            foreach ($_SESSION['cart']['arrCart'] as $key) {
              $total = $total + $key['hrg'];
            }
          }
          echo "</tr>";
          echo "</br>";
        } else
          echo "cart kosong";
        ?>
      </tbody>
    </table>
    <br>
    <h5>Total Pembayaran Rp <?php echo number_format($total, 0,)." JT"; ?> </h5>
    <br><br>
    <div>
      <a class="btn btn-danger btn-lg active text-center" href=remove.php>Hapus</a>
    </div>
    </br>
  </div>

  <!--footer-->
  <div class="footer fixed-bottom">
    <footer class="footer navbar-fixed-bottom bg-info fw-bold text-dark mt-4">
      <div class="text-center p-5">
        Rizal Fadlullah © 2022 Copyright
      </div>
    </footer>
  </div>
  <!--end footer-->

</body>

</html>