<?php
session_start();
if (empty($_SESSION['cart']["arrCart"]))
    $_SESSION['cart']["arrCart"] = array();
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Tokoku</title>

</head>

<body class="d-flex h-100 text-center flex-column">

    <!-- Navbar -->
    <nav id="navbar-top" class="navbar navbar-expand-md navbar-light bg-info mb-5">
        <a class="navbar-brand ms-5 fs-2 p-4 fw-bold" href="#">Tokoku</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample04">
            <ul class="navbar-nav ms-5 me-5 fs-4">
                <li class="nav-item active">
                    <a class="nav-link" href="#home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="cart-disp.php">Cart</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->

    <h2 class="mt-4 fw-bold">JUAL LAPTOP</h2>

    <!-- Product -->
    <div class="row d-flex justify-content-center row-cols-1 row-cols-md-2 g-4 mt-2">
        <div class="card bg-light shadow col-sm-4" style="width: 15rem; right: 5rem;">
            <img src='asus.jpg' class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">ASUS</h5>
                <p class="card-text"> Detail Produk:
                    <br> RAM 32 GB
                    <br> 512 SSD
                    <br> CORE I3
                    <br> Ready
                </p>
                <p class="lead bg-info">Rp. 4JT</p>
                <a href="addCart.php?brg=ASUS&hrg=4JT&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
        <div class="card bg-light shadow col-sm-4" style="width: 15rem; right: 2.5rem;">
            <img src='lenovo.jpg' class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">LENOVO</h5>
                <p class="card-text">Detail Produk:
                    <br> RAM 8 GB
                    <br> 1TB HDD
                    <br> RYZEN 3
                    <br> Ready
                </p>
                <p class="lead bg-info">Rp. 6JT</p>
                <a href="addCart.php?brg=LENOVO&hrg=6JT&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
        <div class="card bg-light shadow col-sm-4" style="width: 15rem;">
            <img src='hp.jpg' class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">HP</h5>
                <p class="card-text">Detail Produk:
                    <br> RAM 8 GB
                    <br> 1TB HDD
                    <br> RYZEN 3
                    <br> Ready
                </p>
                </p>
                <p class="lead bg-info">Rp. 6JT</p>
                <a href="addCart.php?brg=HP&hrg=6JT&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
        <div class="card bg-light shadow col-sm-4" style="width: 15rem; left: 2rem;">
            <img src='razor.jpg' class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">RAZOR</h5>
                <p class="card-text">Detail Produk:
                    <br> RAM 8 GB
                    <br> 1TB HDD
                    <br> RYZEN 3
                    <br> Ready
                </p>
                <p class="lead bg-info">Rp. 7JT</p>
                <a href="addCart.php?brg=RAZOR &hrg=7JT&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
    </div>
    <!-- End Product-->

    <!--footer-->
    <footer class="bg-info fw-bold text-dark mt-5">
        <div class="text-center p-5">
            Rizal Fadlullah © 2022 Copyright
        </div>
    </footer>
    <!--end footer-->

</body>

</html>