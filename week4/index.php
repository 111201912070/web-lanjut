<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Form Pendataan</title>
</head>

<body>
  <h1>Tugas ke-3</h1>
  <form action="simpandata.php" method="post">
    <h2>Form Pendataan</h2><br />
    <label>Nama Lengkap</label><br/>
    <input type="text" class="input" name="name"><br />
    <label>Username</label><br />
    <input type="text" class="input" name="username"><br />
    <label>Email</label><br />
    <input type="email" id="email" name="email" class="input"><br />
    <label>Alamat</label><br />
    <textarea name="address" class="textarea"></textarea><br><br />
    <label>Jenis Kelamin</label><br />
    <input type="radio" name="sex" value="pria" checked>Pria&nbsp;&nbsp;<br />
    <input type="radio" name="sex" value="wanita">Wanita<br />
    <label>Hobi</label><br />
    <input type="checkbox" name="hobby1" value="Tidur"> Tidur<br />
    <input type="checkbox" name="hobby2" value="Fotografi"> Fotografi<br />
    <input type="checkbox" name="hobby3" value="Hunting Sunrise">Hunting Sunrise<br />
    <label>Pekerjaan</label><br/>
    <select name="profession">
      <option value="">Select </option>
      <option value="Sudah Kerja ">Sudah Kerja </option>
      <option value="Belum Kerja">Belum Kerja </option>
      <option value="Sedang Mencari & Menunggu">Sedang Mencari & Menunggu</option>
    </select><br/>
    <input type="submit" value="Simpan" class="btn">
  </form>
</body>

</html>