<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir Mahasiswa</title>
</head>
<body>
    <form action="AksiMahasiswa.php" method="POST">
        <table>
        <tr>
            <td><label for="nama">Nama Lengkap</label></td>
            <td><input type="text" name="nama" placeholder="Input Lengkap"></td>
        </tr>
        <tr>
            <td><label for="nime">NIM</label></td>
            <td><input type="text" name="nim" placeholder="Input NIM"></td>
        </tr>
        <tr>
            <td><label for="alamat">Alamat</label></td>
            <td><input type="text" name="alamat" placeholder="Alamat"></td>
        </tr>
        <tr>
            <td><label for="prodi">Prodi</label></td>
            <td><select name="prodi">
                <option>Pilih Prodi</option>
                <option value="A11">Teknik Informatika</option>
                <option value="A22">Sistem Informasi</option>
                <option value="B11">Akuntansi</option>
                <option value="B22">Management</option>
            </select></td>
        </tr>
        <tr>
            <td><button type="submit">Submit</button></td>
        </tr>
        </table>
    </form>
</body>
</html>
